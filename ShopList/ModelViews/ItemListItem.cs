using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;

namespace ShopList.ModelViews
{
    public class ItemListItem
    {
        public Item Item { get; set; }
        public ListItem ListItem { get; set; }
    }
}