using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShopList.UI.Behavior
{
    public class BehaviorButtonMenu : IBehavior<View>
    {
        public View View
        {
            set
            {
                value.Click += Click;
            }
        }

        private void Click(object sender, EventArgs e)
        {
            View view = (View)sender;

            //temp: switching between activities. Future: menu for selecting different activities / submenus. 
            Type t = view.Context.GetType();
            Intent i = new Intent(view.Context, t == typeof(ActivityMain) ? typeof(ActivityItemsRaw) : typeof(ActivityMain));
            view.Context.StartActivity(i);
        }
    }
}