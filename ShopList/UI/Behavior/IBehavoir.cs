using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;

namespace ShopList.UI.Behavior
{
    //this should be the new IBehavior, but for now IBehavior is being left alone to satisfy the compiler to allow gradual change
    public interface IBehavior<TEntity> where TEntity : View
    {
        TEntity View { set; }
    }
}