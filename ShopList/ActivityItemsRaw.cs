using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.DAL;
using ShopList.Models;
using ShopList.UI.Fragments;
using ShopList.UI.Behavior;
using ShopList.UI.Adapters;

namespace ShopList
{
    [Activity(Label = "Items", LaunchMode = Android.Content.PM.LaunchMode.SingleInstance)]
    public class ActivityItemsRaw : Activity, IActivity
    {
        public IUnitOfWork UW { get; private set; }

        private int ThemeID { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            ThemeID = Resource.Style.ThemeLight;
            this.SetTheme(ThemeID);
            SetContentView(Resource.Layout.FrameHorizontalTriple);

            UW = new UnitOfWork();

            var frameTop = FragmentManager.FindFragmentById(Resource.Id.Frame01) as FragmentMenuBarHorizontal<List>;
            List list = UW.ListRepo.GetAll().FirstOrDefault<List>();
            if (frameTop == null) frameTop = new FragmentMenuBarHorizontal<List>(list, new BehaviorButtonMenu(), ThemeID);

            var frameMiddle = FragmentManager.FindFragmentById(Resource.Id.Frame02) as FragmentList<Item, View>;
            IEnumerable<Item> items = UW.ItemRepo.GetAll();
            if (frameMiddle == null) frameMiddle = new FragmentList<Item, View>(new AdapterItem(this, items), new BehaviorItem(), ThemeID);

            var frameBottom = FragmentManager.FindFragmentById(Resource.Id.Frame02) as FragmentItemEdit;
            if (frameBottom == null) frameBottom = new FragmentItemEdit(ThemeID);

            FragmentControl.CreateFragment(FragmentManager, Resource.Id.Frame01, frameTop);
            FragmentControl.CreateFragment(FragmentManager, Resource.Id.Frame02, frameMiddle);
            FragmentControl.CreateFragment(FragmentManager, Resource.Id.Frame03, frameBottom);

        }
    }
}