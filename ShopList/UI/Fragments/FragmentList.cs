using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ShopList.DAL;
using ShopList.Models;
using System.Collections;
using ShopList.UI.Adapters;
using ShopList.UI.Behavior;

namespace ShopList.UI.Fragments
{
    public class FragmentList<TEntity, TEntity2> : Android.App.ListFragment where TEntity : class where TEntity2 : View
    {
        public IEnumerable<TEntity> Collection { get; private set; }

        private IBehavior<TEntity2> Behavior { get; set; }
        private AAdapter<TEntity> BasicAdapter { get; set; }

        private int Theme { get; set; }


        public FragmentList(AAdapter<TEntity> adapter, IBehavior<TEntity2> behavior)
        {
            Initialize(adapter, behavior, -1);
        }

        public FragmentList(AAdapter<TEntity> adapter, IBehavior<TEntity2> behavior, int theme)
        {
            Initialize(adapter, behavior, theme);
        }

        private void Initialize(AAdapter<TEntity> adapter, IBehavior<TEntity2> behavior, int theme)
        {
            Behavior = behavior;
            BasicAdapter = adapter;
            Theme = theme;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            if (Theme != -1) Activity.SetTheme(Theme);
            this.ListAdapter = BasicAdapter;

            //Behavior.View = //all views from the adapter. Not currently available.
        }
    }
}