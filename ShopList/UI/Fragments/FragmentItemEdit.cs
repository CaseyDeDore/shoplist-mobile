using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ShopList.Models;

namespace ShopList.UI.Fragments
{
    public class FragmentItemEdit : Fragment
    {
        public Item Item { get; set; }

        private int Theme { get; set; }

        private const int DEFAULT_THEME_ID = -111;


        public FragmentItemEdit(int t = DEFAULT_THEME_ID)
        {
            Theme = t;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (Theme != DEFAULT_THEME_ID) Activity.SetTheme(Theme);
            return inflater.Inflate(Resource.Layout.ItemEditName, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            //do stuffs here
        }
    }
}