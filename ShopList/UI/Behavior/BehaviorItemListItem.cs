using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using ShopList.DAL;
using ShopList.ModelViews;
using ShopList.Logic;
using Android.Util;

namespace ShopList.UI.Behavior
{
    public class BehaviorItemListItem : IBehavior<CheckedTextView, ItemListItem>
    {
        public ItemListItem Entity { get; set; }

        public CheckedTextView View
        {
            set
            {
                value.Click += Click;
            }
        }

        private void Click(object sender, EventArgs e)
        {
            var view = (CheckedTextView)sender;

            var logic = new LogicListItem();
            logic.ItemCheckToggle(Entity.ListItem);

            view.Toggle();
        }
    }
}