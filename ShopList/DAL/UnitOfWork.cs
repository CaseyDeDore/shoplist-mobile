using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using ShopList.DAL;
using SQLite;
using System.IO;


namespace ShopList.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public string DatabasePath { get; private set; }

        private static SQLiteConnection Connection { get; set; }

        private static IRepository<List> listRepo;
        private static IRepository<Item> itemRepo;
        private static IRepository<Category> categoryRepo;
        private static IRepository<ListItem> listItemRepo;
        private static IRepository<CategoryItem> categoryItemRepo;
        private static IRepository<Measurement> measurementRepo;
        private static IRepository<MeasurementItem> measurementItemRepo;


        public UnitOfWork(string path = null)
        {
            #if __ANDROID__
            try
            {
                if (path == null) DatabasePath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "10392302helloshop.db3");
                else DatabasePath = path;

                Connection = new SQLiteConnection(DatabasePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endif
        }

        public IRepository<List> ListRepo
        {
            get { if (listRepo == null) listRepo = new Repository<List>(Connection); return listRepo; }
        }

        public IRepository<Item> ItemRepo
        {
            get { if (itemRepo == null) itemRepo = new Repository<Item>(Connection); return itemRepo; }
        }

        public IRepository<Category> CategoryRepo
        {
            get { if (categoryRepo == null) categoryRepo = new Repository<Category>(Connection); return categoryRepo; }
        }

        public IRepository<ListItem> ListItemRepo
        {
            get { if (listItemRepo == null) listItemRepo = new Repository<ListItem>(Connection); return listItemRepo; }
        }

        public IRepository<CategoryItem> CategoryItemRepo
        {
            get { if (categoryItemRepo == null) categoryItemRepo = new Repository<CategoryItem>(Connection); return categoryItemRepo; }
        }

        public IRepository<Measurement> MeasurementRepo
        {
            get { if (measurementRepo == null) measurementRepo = new Repository<Measurement>(Connection); return measurementRepo; }
        }

        public IRepository<MeasurementItem> MeasurementItemRepo
        {
            get { if (measurementItemRepo == null) measurementItemRepo = new Repository<MeasurementItem>(Connection); return measurementItemRepo; }
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }
    }
}