using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using ShopList.UI.Fragments;
using Android.Views.InputMethods;
using ShopList.DAL;

namespace ShopList.UI.Behavior
{
    public class BehaviorItem : IBehavior<View, Item>
    {
        public Item Entity { get; set; }

        public View View
        {
            set
            {
                value.Click += Click;
                value.LongClick += ClickLong;
            }
        }


        private void Click(object sender, EventArgs e)
        {
            if (Entity == null)
            {
                Entity = Entity;
            }
            else
            {
                UnitOfWork UW = new UnitOfWork();
                //this text currently comes back as and empty string
                //CurrentItem.Name = txtEdit.Text;
                UW.ItemRepo.Update(Entity);
                Entity = null;
            }
        }

        private void ClickLong(object sender, View.LongClickEventArgs e)
        {
            //temp delete. Really crappy for user right now.
            UnitOfWork UW = new UnitOfWork();
            var itemsInLists = from ListItem i in UW.ListItemRepo.GetAll()
                               where i.ItemID == Entity.ID
                               select i;

            UW.ItemRepo.Delete(Entity);

            if (itemsInLists.Count() <= 0) return;

            foreach (var i in itemsInLists)
            {
                UW.ListItemRepo.Delete(i);
            }
        }
    }
}