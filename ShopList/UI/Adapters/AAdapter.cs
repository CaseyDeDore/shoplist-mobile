using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections;
using ShopList.Models;

namespace ShopList.UI.Adapters
{
    public abstract class AAdapter<TEntity> : BaseAdapter
    {
        protected Activity Activity { get; set; }
        public List<TEntity> Items { get; protected set; }

        
        public AAdapter(Activity act, IEnumerable items)
        {
            Activity = act;
            Items = items.Cast<TEntity>().ToList<TEntity>();
        }

        public override int Count
        {
            get { return Items.Count(); }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return Items[position] as Java.Lang.Object;
        }

        protected virtual TEntity GetItemByIndex(int pos)
        {
            return Items[pos];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public abstract override View GetView(int position, View convertView, ViewGroup parent);
    }
}