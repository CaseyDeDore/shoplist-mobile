using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.DAL;
using ShopList.Models;

namespace ShopList.DAL
{
    public interface IUnitOfWork
    {
        IRepository<List> ListRepo { get; }
        IRepository<Item> ItemRepo { get; }
        IRepository<Category> CategoryRepo { get; }
        IRepository<ListItem> ListItemRepo { get; }
        IRepository<CategoryItem> CategoryItemRepo { get; }
        IRepository<Measurement> MeasurementRepo { get; }
        IRepository<MeasurementItem> MeasurementItemRepo { get; }

        bool Save();
    }
}