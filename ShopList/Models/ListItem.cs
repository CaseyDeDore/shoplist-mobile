﻿using SQLite;
using System.Collections;

namespace ShopList.Models
{
    public class ListItem
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }

        public int ListID { get; set; }

        public int ItemID { get; set; }

        public bool Checked { get; set; }

        public Priority  Priority{ get; set; }
    }
}
