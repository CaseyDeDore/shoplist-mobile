﻿using SQLite;
using System.Collections;

namespace ShopList.Models
{
    public class CategoryItem
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
        public int CategoryID { get; set; }

        public int ItemID { get; set; }
    }
}
