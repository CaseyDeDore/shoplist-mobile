using System;
using System.Collections;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using System.Collections.Generic;

namespace ShopList.UI.Adapters
{
    public class AdapterItem : AAdapterDatabaseEntity<Item>
    {
        public AdapterItem(Activity act, IEnumerable items) : base(act, items) { }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? Activity.LayoutInflater.Inflate(Resource.Layout.AdapterItem, parent, false);
            var info = view.FindViewById<TextView>(Resource.Id.TxtItem);

            info.Text = String.Format(info.Text, Items[position].Name);

            return view;
        }
    }
}