using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace ShopList.Models
{
    public class MeasurementItem
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
        public int MeasurementID { get; set; }

        public int ItemID { get; set; }
    }
}