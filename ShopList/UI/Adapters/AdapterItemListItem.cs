using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using System.Collections;
using ShopList.ModelViews;

namespace ShopList.UI.Adapters
{
    public class AdapterItemListItem : AAdapter<ItemListItem>
    {
        public AdapterItemListItem(Activity act, IEnumerable items) : base(act, items) { }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? Activity.LayoutInflater.Inflate(Resource.Layout.AdapterItemListItem, parent, false);
            var info = view.FindViewById<CheckedTextView>(Resource.Id.TxtChkItem);

            info.Text = String.Format(info.Text, Items[position].Item.Name);
            info.Checked = Items[position].ListItem.Checked;

            return view;
        }
    }
}