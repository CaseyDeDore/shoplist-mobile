using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.DAL;
using ShopList.Models;

namespace ShopList.Logic
{
    public class LogicListItem
    {
        private IUnitOfWork UW { get; set; }


        public LogicListItem()
        {
            UW = new UnitOfWork();
        }

        public LogicListItem(IUnitOfWork u)
        {
            UW = u;
        }

        public bool ItemCheckToggle(ListItem listItemToCheck)
        {
            listItemToCheck.Checked = !listItemToCheck.Checked;

            UW.ListItemRepo.Update(listItemToCheck);

            return listItemToCheck.Checked;
        }
    }
}