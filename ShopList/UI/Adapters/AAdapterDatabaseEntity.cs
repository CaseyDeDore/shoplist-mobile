using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using System.Collections;

namespace ShopList.UI.Adapters
{
    public abstract class AAdapterDatabaseEntity<TEntity> : AAdapter<TEntity> where TEntity : IDatabaseEntity
    {
        public AAdapterDatabaseEntity(Activity act, IEnumerable items) : base(act, items) { }

        public override long GetItemId(int position)
        {
            return GetItemByIndex(position).ID;
        }
    }
}