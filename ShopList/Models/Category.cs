﻿using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopList.Models
{
    public class Category : IDatabaseEntity
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
