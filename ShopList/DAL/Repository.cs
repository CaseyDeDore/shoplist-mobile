﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ShopList.DAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        public string DatabasePath { get; private set; }

        private SQLiteConnection Connection { get; set; }

        public Repository(SQLiteConnection connection)
        {
            Connection = connection;

            try
            {
                Connection.CreateTable<TEntity>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Connection.Table<TEntity>() as IEnumerable<TEntity>;
        }

        public IEnumerable<TEntity> Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
            //actually, sqlite Get() only returns the first. So far has only come back null. 
            //return Connection.Get<TEntity>(predicate) as IEnumerable<TEntity>;
        }

        public TEntity Get(object id)
        {
            return Connection.Get<TEntity>((int)id);
        }

        public void Insert(TEntity entity)
        {
            Connection.Insert(entity);
        }

        public void Delete(object id)
        {
            Connection.Delete((int)id);
        }

        public void Delete(TEntity entityToDelete)
        {
            Connection.Delete(entityToDelete);
        }

        public void Update(TEntity entityToUpdate)
        {
            Connection.Update(entityToUpdate);
        }

        public int Count()
        {
            return Connection.Table<TEntity>().Count();
        }
    }
}
