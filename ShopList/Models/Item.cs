﻿using SQLite;
using System.Collections;

namespace ShopList.Models
{
    public class Item : IDatabaseEntity
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }

        public string Name { get; set; }
    }
}
