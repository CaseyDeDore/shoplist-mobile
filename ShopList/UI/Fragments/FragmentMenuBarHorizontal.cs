using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using ShopList.UI.Behavior;

namespace ShopList.UI.Fragments
{
    public class FragmentMenuBarHorizontal<TEntity> : Fragment where TEntity : IDatabaseEntity
    {
        public TEntity Item { get; private set; }

        private IBehavior<View> BehaviorButtonMenu { get; set; }

        private int Theme { get; set; }

        private const int DEFAULT_THEME_ID = -111;


        public FragmentMenuBarHorizontal(TEntity item, IBehavior<View> menuButtonBehavior)
        {
            Item = item;
            BehaviorButtonMenu = menuButtonBehavior;
            Theme = DEFAULT_THEME_ID;
        }

        public FragmentMenuBarHorizontal(TEntity item, IBehavior<View> menuButtonBehavior, int t)
        {
            Item = item;
            BehaviorButtonMenu = menuButtonBehavior;
            Theme = t;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if(Theme != DEFAULT_THEME_ID) Activity.SetTheme(Theme);
            return inflater.Inflate(Resource.Layout.MenuBarHorizontal, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            var title = Activity.FindViewById<TextView>(Resource.Id.txtTitle);
            var button = Activity.FindViewById<ImageButton>(Resource.Id.BtnMenu);

            title.Text = Item.Name;
            BehaviorButtonMenu.View = button;
        }
    }
}