﻿using SQLite;
using System.Collections;

namespace ShopList.Models
{
    public class List : IDatabaseEntity
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
