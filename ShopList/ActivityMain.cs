﻿using System;
using System.Linq;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using ShopList.DAL;
using ShopList.Models;
using ShopList.UI.Fragments;
using ShopList.Logic;
using ShopList.UI.Behavior;
using ShopList.ModelViews;
using ShopList.UI.Adapters;

namespace ShopList
{
    [Activity(Label = "ShopList", MainLauncher = true, Icon = "@drawable/icon", LaunchMode = Android.Content.PM.LaunchMode.SingleInstance)]
    public class ActivityMain : Activity, IActivity
    {
        public IUnitOfWork UW { get; set; }

        private int ThemeID { get; set; }


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            ThemeID = Resource.Style.ThemeLight;
            this.SetTheme(ThemeID);
            SetContentView(Resource.Layout.FrameHorizontalDouble);

            try
            {
                UW = new UnitOfWork();
                DatabaseInitializer.PopulateDatabase(UW);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var frameTop = FragmentManager.FindFragmentById(Resource.Id.Frame01) as FragmentMenuBarHorizontal<List>;
            List list = UW.ListRepo.GetAll().FirstOrDefault<List>();
            if(frameTop == null) frameTop = new FragmentMenuBarHorizontal<List>(list, new BehaviorButtonMenu(), ThemeID);

            var frameBottom = FragmentManager.FindFragmentById(Resource.Id.Frame02) as FragmentList<ItemListItem, CheckedTextView>;


            IEnumerable<ListItem> listItems = from ListItem li in UW.ListItemRepo.GetAll()
                                             where li.ListID == list.ID
                                             select li;

            IEnumerable<Item> items = from Item i in UW.ItemRepo.GetAll()
                                      join ListItem li in listItems on i.ID equals li.ItemID
                                      select i;

            List<ItemListItem> itemListItems = new List<ItemListItem>();

            for(var i = 0; i < listItems.Count(); i++)
            {
                itemListItems.Add(new ItemListItem() { ListItem = listItems.ElementAtOrDefault(i), Item = items.ElementAtOrDefault(i) });
            }

            if(frameBottom == null) frameBottom = 
                new FragmentList<ItemListItem, CheckedTextView>(new AdapterItemListItem(this, itemListItems), new BehaviorItemListItem(), ThemeID);


            FragmentControl.CreateFragment(this.FragmentManager, Resource.Id.Frame01, frameTop);

            FragmentControl.CreateFragment(this.FragmentManager, Resource.Id.Frame02, frameBottom);
        }
    }
}

