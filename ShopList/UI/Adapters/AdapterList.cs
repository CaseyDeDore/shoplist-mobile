using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.Models;
using System.Collections;

namespace ShopList.UI.Adapters
{
    public class AdapterList : AAdapterDatabaseEntity<List>
    {
        public AdapterList(Activity act, IEnumerable items) : base(act, items) { }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //this is temporary until list gets a layout just for itself
            var view = convertView ?? Activity.LayoutInflater.Inflate(Resource.Layout.AdapterItem, parent, false);
            var info = view.FindViewById<TextView>(Resource.Id.TxtItem);

            info.Text = String.Format(info.Text, Items[position].Name);

            return view;
        }
    }
}