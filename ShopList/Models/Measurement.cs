using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace ShopList.Models
{
    public class Measurement : IDatabaseEntity
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
        public string Name{ get; set; }
    }
}

//Measurment will hold user-defined values for the amounts of things that an item is needed in. It will be coupled with Item in shopping lists.