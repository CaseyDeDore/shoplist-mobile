using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShopList.UI.Behavior
{
    public interface IBehavior<TEntity, TEntity2> : IBehavior<TEntity> where TEntity : View where TEntity2 : class
    {
        TEntity2 Entity { get; set; }
    }
}