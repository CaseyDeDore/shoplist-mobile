using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShopList.DAL;
using ShopList.Models;
using System.Collections;

namespace ShopList.Logic
{
    public class DatabaseInitializer
    {
        private static IUnitOfWork UW { get; set; }


        public static void PopulateDatabase(IUnitOfWork uw)
        {
            UW = uw;

            var items = UW.ItemRepo;
            var lists = UW.ListRepo;
            var listItems = UW.ListItemRepo;
            var measurements = UW.MeasurementRepo;
            var measurementItems = UW.MeasurementItemRepo;
            var categories = UW.CategoryRepo;
            var categoryItems = UW.CategoryItemRepo;

            IEnumerable itemsCache = items.GetAll().Cast<Item>().ToList<Item>();

            Random rand = new Random();

            if (items.Count() <= 0)
            {
                items.Insert(new Item() { Name = "Partunias" });
                items.Insert(new Item() { Name = "Bottled Starfish" });
                items.Insert(new Item() { Name = "Kraft Bear" });
                items.Insert(new Item() { Name = "Cheezos" });
                items.Insert(new Item() { Name = "A Rift in the Space Time Continuum" });
                items.Insert(new Item() { Name = "The Thing" });
                items.Insert(new Item() { Name = "Jangles The Moon Monkey" });
                items.Insert(new Item() { Name = "Rhode Island Fried Chicken" });
                items.Insert(new Item() { Name = "Roman Alphabet Soup" });
                items.Insert(new Item() { Name = "A Container of Fresh Air" });
                items.Insert(new Item() { Name = "The Cookbook From The Movie Ratatouille" });
                items.Insert(new Item() { Name = "Rock Flavored Toothwax" });
                items.Insert(new Item() { Name = "Candied Cheesecloth" });
                items.Insert(new Item() { Name = "Soylent Blue" });
                items.Insert(new Item() { Name = "Johnson's Baby Bloopers" });

                itemsCache = items.GetAll().Cast<Item>().ToList<Item>();
            }

            if (lists.Count() <= 0)
            {
                lists.Insert(new List() { Name = "Test List"});
                lists.Insert(new List() { Name = "Empty List" });
            }

            if (listItems.Count() <= 0)
            {
                int listID = lists.GetAll().Cast<List>().ToList<List>()[0].ID;

                foreach (Item i in itemsCache)
                {
                    listItems.Insert(new ListItem() { ListID = listID, ItemID = i.ID });
                }
            }

            if (measurements.Count() <= 0)
            {
                measurements.Insert(new Measurement() { Name = "lb" });
                measurements.Insert(new Measurement() { Name = "cup" });
                measurements.Insert(new Measurement() { Name = "oz" });
            }

            if (measurementItems.Count() <= 0)
            {
                var m = measurements.GetAll().Cast<Measurement>().ToList<Measurement>();

                foreach (Item i in itemsCache)
                {
                    measurementItems.Insert(new MeasurementItem() { MeasurementID = m[rand.Next(0, m.Count())].ID, ItemID = i.ID });
                }
            }

            if (categories.Count() <= 0)
            {
                categories.Insert(new Category() { Name = "Item Category" });
            }

            if (categoryItems.Count() <= 0)
            {
                var c = categories.GetAll().Cast<Category>().ToList<Category>();

                foreach (Item i in itemsCache)
                {
                    categoryItems.Insert(new CategoryItem() { CategoryID = c[rand.Next(0, c.Count())].ID, ItemID = i.ID });
                }
            }
        }
    }
}