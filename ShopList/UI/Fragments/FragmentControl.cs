using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShopList.UI.Fragments
{
    public static class FragmentControl
    {
        public static void CreateFragment(FragmentManager manager, int viewContainerId, Fragment fragment, FragmentTransit transition = FragmentTransit.None)
        {
            try
            {
                var trans = manager.BeginTransaction();
                trans.Replace(viewContainerId, fragment);
                trans.SetTransition(transition);
                trans.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}